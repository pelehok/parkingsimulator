using System;

namespace ParkingSimulator.Core.Exceptions.ParkingException
{
    public class VehicleHasPenaltyException : Exception
    {
        public VehicleHasPenaltyException(string message) : base(message)
        {
        }
    }
}