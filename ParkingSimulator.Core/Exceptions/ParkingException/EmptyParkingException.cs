using System;

namespace ParkingSimulator.Core.Exceptions.ParkingException
{
    public class EmptyParkingException: Exception
    {
        public EmptyParkingException(string message) : base(message)
        {
        }
    }
}