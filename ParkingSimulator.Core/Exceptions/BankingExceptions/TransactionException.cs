using System;

namespace ParkingSimulator.Core.Exceptions.BankingExceptions
{
	public class TransactionException : Exception
	{
		public TransactionException(string message) : base(message)
		{
		}
	}
}