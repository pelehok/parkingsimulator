using System;

namespace ParkingSimulator.Core.Exceptions.BankingExceptions
{
	public class InvalidAmountException : Exception
	{
		public InvalidAmountException(string message) : base(message)
		{
		}
	}
}