namespace ParkingSimulator.Core
{
	public class ParkingSimulatorSettings
	{
		public static int MaxParkingSize = 10;
		public static double DefaultParkingBalance = 10;
		public static double DefaultVehicleBalance = 10;
		public static int RentalTimeSec = 5;

		public static double RentalRateForCar = 2;
		public static double RentalRateForMotorBike = 1;
		public static double RentalRateForVan = 5;
		public static double RentalRateForBus = 3.5;
		public static double PenaltyCoefficient = 1.5;
	}
}