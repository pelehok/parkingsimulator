using System.Timers;

namespace ParkingSimulator.Core.Wrappers
{
    public class TimerWrapper : ITimerWrapper
    {
        private Timer _timer;
        public event ElapsedEventHandler Elapsed;
        
        public TimerWrapper(int interval)
        {
            _timer = new Timer(interval);
            _timer.Elapsed += Tick;
        }

        private void Tick(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, e);
        }

        public void Start()
        {
            _timer.Start();
        }
        
    }
}