using System.Timers;

namespace ParkingSimulator.Core.Wrappers
{
    public interface ITimerWrapper
    {
        event ElapsedEventHandler Elapsed;
        void Start();
    }
}