using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class Bus : Vehicle
	{
		public override string ToString()
		{
			return $"Bus {base.ToString()}";
		}

		public Bus(IAccount account) : base(account)
		{
		}
	}
}