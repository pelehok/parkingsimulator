using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class Car : Vehicle
	{
		public override string ToString()
		{
			return $"Car {base.ToString()}";
		}

		public Car(IAccount account) : base(account)
		{
		}
	}
}