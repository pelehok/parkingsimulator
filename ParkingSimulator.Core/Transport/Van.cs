using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class Van : Vehicle
	{
		public override string ToString()
		{
			return $"Van {base.ToString()}";
		}

		public Van(IAccount account) : base(account)
		{
		}
	}
}