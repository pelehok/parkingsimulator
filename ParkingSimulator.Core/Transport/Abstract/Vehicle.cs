using ParkingSimulator.Core.Banking.Abstract;

namespace ParkingSimulator.Core.Transport.Abstract
{
	public abstract class Vehicle
	{
		public IAccount BankAccount { get; set; }
		public string VehicleName { get; set; }

		public Vehicle(IAccount account)
		{
			BankAccount = account;
		}

		public override string ToString()
		{
			return $"{VehicleName}. Balance {BankAccount.Balance}";
		}
	}
}