using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.Transport
{
	public class MotorBike : Vehicle
	{
		public override string ToString()
		{
			return $"Motor Bike {base.ToString()}";
		}

		public MotorBike(IAccount account) : base(account)
		{
		}
	}
}