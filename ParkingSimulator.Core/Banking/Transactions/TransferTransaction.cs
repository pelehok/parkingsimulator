using System;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Transactions
{
	public class TransferTransaction : ITransaction
	{
		public double Amount { get; }
		public DateTime TransactionTime { get; }
		
		public TransferTransaction(double amount)
		{
			TransactionTime = DateTime.Now;
			Amount = amount;
		}

		private const string TransactionInformation = "Transfet transaction amount = ";
		public string GetTransactionInfo()
		{
			return $"{TransactionInformation}{Amount} at {TransactionTime.ToString()}";
		}
	}
}