using System;

namespace ParkingSimulator.Core.Banking.Transactions.Abstract
{
	public interface ITransaction
	{
		string GetTransactionInfo();
		
		double Amount { get; }
		
		DateTime TransactionTime { get; }
	}
}