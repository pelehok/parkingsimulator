using System;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Transactions
{
	public class WithdrawTransaction : ITransaction
	{
		public double Amount { get; }
		public DateTime TransactionTime { get; }

		public WithdrawTransaction(double amount)
		{
			TransactionTime = DateTime.Now;
			Amount = amount;
		}

		private const string TransactionInformation = "Withdraw transaction amount = ";
		public string GetTransactionInfo()
		{
			return $"{TransactionInformation}{Amount} at {TransactionTime}";
		}
	}
}