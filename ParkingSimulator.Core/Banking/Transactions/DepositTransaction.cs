using System;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Transactions
{
	public class DepositTransaction : ITransaction
	{
		public double Amount { get; }
		public DateTime TransactionTime { get; }
		
		public DepositTransaction(double amount)
		{
			TransactionTime = DateTime.Now;
			Amount = amount;
		}

		private const string TransactionInformation = "Deposit transaction amount = ";
		public string GetTransactionInfo()
		{
			return $"{TransactionInformation}{Amount} at {TransactionTime.ToString()}";
		}

	}
}