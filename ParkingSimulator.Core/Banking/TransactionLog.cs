using System.Collections.Generic;
using System.Text;
using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking
{
	public class TransactionLog : ITransactionLog
	{
		private List<ITransaction> _transaction { get; set; }

		public TransactionLog()
		{
			_transaction = new List<ITransaction>();
		}

		public void AddTransaction(ITransaction transaction)
		{
			_transaction.Add(transaction);
		}

		public override string ToString()
		{
			StringBuilder logInformation = new StringBuilder("");
			foreach (var transaction in _transaction)
			{
				logInformation.Append($"{transaction.GetTransactionInfo()}\r\n");
			}

			return logInformation.ToString();
		}
	}
}