using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Banking.Transactions;
using ParkingSimulator.Core.Exceptions.BankingExceptions;

namespace ParkingSimulator.Core.Banking
{
	public class Account : IAccount
	{
		private ITransactionLog _transactionLog;

		public string Name { get; set; }
		public double Balance { get; private set; }

		public Account(ITransactionLog transactionLog)
		{
			_transactionLog = transactionLog;
		}
		
		public void Deposit(double amount)
		{
			if(amount<=0)
			{
				throw new InvalidAmountException("");
			}

			Balance += amount;
			_transactionLog.AddTransaction(new DepositTransaction(amount));
		}

		public void Withdraw(double amount)
		{
			if (Balance < amount)
			{
				throw new InvalidAmountException("");
			}
			else if(amount<=0)
			{
				throw new InvalidAmountException("");
			}

			Balance -= amount;
			_transactionLog.AddTransaction(new WithdrawTransaction(amount));
		}

		public void TransferFunds(IAccount destination, double amount)
		{
			Withdraw(amount);
			destination.Deposit(amount);
			_transactionLog.AddTransaction(new TransferTransaction(amount));
		}

		public string GetTransactionInfo()
		{
			return _transactionLog.ToString();
		}
	}
}