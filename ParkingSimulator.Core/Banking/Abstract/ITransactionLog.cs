using ParkingSimulator.Core.Banking.Transactions.Abstract;

namespace ParkingSimulator.Core.Banking.Abstract
{
    public interface ITransactionLog
    {
        void AddTransaction(ITransaction transaction);
    }
}