namespace ParkingSimulator.Core.Banking.Abstract
{
	public interface IAccount
	{
		string Name { get; set; }
		double Balance { get; }
		void Deposit(double amount);
		void Withdraw(double amount);
		void TransferFunds(IAccount destination, double amount);
		string GetTransactionInfo();
	}
}