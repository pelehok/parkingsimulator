using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Exceptions.BankingExceptions;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.ParkingLogic
{
    public class ParkingCell
    {
        public Vehicle Vehicle { get; }
        public bool IsHavePenalty { get; private set; }

        private double _rentalRate;
        private double _penaltyValue;

        public ParkingCell(Vehicle vehicle, double rentalRate)
        {
            Vehicle = vehicle;
            _rentalRate = rentalRate;
            IsHavePenalty = false;
        }


        public void RentPayment(IAccount transferTo)
        {
            if (IsHavePenalty)
            {
                _penaltyValue += _rentalRate * ParkingSimulatorSettings.PenaltyCoefficient;
                try
                {
                    Vehicle.BankAccount.TransferFunds(transferTo, _penaltyValue);
                    IsHavePenalty = false;
                }
                catch (InvalidAmountException e)
                {
                    return;
                }
            }
            else
            {
                try
                {
                    Vehicle.BankAccount.TransferFunds(transferTo, _rentalRate);
                }
                catch (InvalidAmountException e)
                {
                    IsHavePenalty = true;
                    _penaltyValue += _rentalRate * ParkingSimulatorSettings.PenaltyCoefficient;
                }
            }
        }
    }
}