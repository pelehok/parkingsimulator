using System;
using System.Collections.Generic;
using System.Linq;

using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Exceptions.ParkingException;
using ParkingSimulator.Core.ParkingLogic.Abstract;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.Core.ParkingLogic
{
	public class Parking
	{
		private readonly int _parkingCapacity = ParkingSimulatorSettings.MaxParkingSize;
		
		public List<ParkingCell> Cells { get; private set; }
		public IAccount BankAccount { get; private set; }
		
		public Parking(IAccount account, IParkingManager parkingManager)
		{
			BankAccount = account;
			BankAccount.Deposit(ParkingSimulatorSettings.DefaultParkingBalance);

			Cells = new List<ParkingCell>();
			
			parkingManager.SetUpParkingData(Cells,BankAccount);
		}

		public void AddVehicle(Vehicle vehicle)
		{
			if (Cells.Count + 1 > _parkingCapacity)
			{
				throw new ParkingFullException("Parking full. Don't have free spaces");
			}

			Cells.Add(new ParkingCell(vehicle, GetRentalRateForVehicle(vehicle)));
		}
		
		public void RemoveVehicle(Vehicle vehicle)
		{
			if (Cells.Count==0)
			{
				throw new EmptyParkingException("Vehicle didn't found");
			}

			var vehicleCell = Cells.FirstOrDefault(x=>x.Vehicle.Equals(vehicle)) ?? throw new ArgumentException("Parking don't have this vehicle");
			if (vehicleCell.IsHavePenalty)
			{
				throw new VehicleHasPenaltyException("Vehicle has penalty. You can't remove it");
			}

			Cells.Remove(vehicleCell);
		}

		public int NumberFreeCells()
		{
			return ParkingSimulatorSettings.MaxParkingSize - Cells.Count;
		}
		
		public int NumberBusyCells()
		{
			return Cells.Count;
		}

		private double GetRentalRateForVehicle(Vehicle vehicle)
		{
			switch (vehicle)
			{
				case Car _:
					return ParkingSimulatorSettings.RentalRateForCar;
				case Bus _:
					return ParkingSimulatorSettings.RentalRateForBus;
				case Van _:
					return ParkingSimulatorSettings.RentalRateForVan;
				case MotorBike _:
					return ParkingSimulatorSettings.RentalRateForMotorBike;
			}

			return 0.0;
		}
	}
}