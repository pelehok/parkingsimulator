using System.Collections.Generic;
using ParkingSimulator.Core.Banking.Abstract;

namespace ParkingSimulator.Core.ParkingLogic.Abstract
{
    public interface IParkingManager
    {
        void SetUpParkingData(List<ParkingCell> cells, IAccount account);
    }
}