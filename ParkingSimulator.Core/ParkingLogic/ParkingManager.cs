using System.Collections.Generic;
using System.Timers;
using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Ioc;
using ParkingSimulator.Core.ParkingLogic.Abstract;
using ParkingSimulator.Core.Wrappers;

namespace ParkingSimulator.Core.ParkingLogic
{
    public class ParkingManager : IParkingManager
    {
        private List<ParkingCell> _cells;
        private ITimerWrapper _timer;
        private IAccount _account;

        public ParkingManager()
        {
            _timer = DependencyInjector.Resolve<ITimerWrapper>();
            _timer.Elapsed += Rental;
        }

        public void SetUpParkingData(List<ParkingCell> cells,IAccount account)
        {
            _cells = cells;
            _account = account;
            
            _timer.Start();
        }
        
        private void Rental(object sender, ElapsedEventArgs e)
        {
            foreach (var cell in _cells)
            {
                cell.RentPayment(_account);
            }
        }
        
    }
}