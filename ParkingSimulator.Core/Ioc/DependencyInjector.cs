using System;
using Unity;
using Unity.Injection;

namespace ParkingSimulator.Core.Ioc
{
    public static class DependencyInjector
    {
        private static readonly UnityContainer UnityContainer = new UnityContainer();
        public static void RegisterType<I, T>() where T : I
        {
            UnityContainer.RegisterType<I, T>();
        }
        public static void RegisterType<I>(InjectionFactory factory)
        {
            UnityContainer.RegisterType<I>(factory);
        }
        public static void RegisterInstance<I>(I instance)
        {
            UnityContainer.RegisterInstance<I>(instance);
        }
        public static T Resolve<T>()
        {
            return UnityContainer.Resolve<T>();
        }
        public static object Retrieve<T>(Type objectType)
        {
            return UnityContainer.Resolve(objectType);
        }
    }
}