using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.ParkingLogic;
using ParkingSimulator.Core.ParkingLogic.Abstract;
using ParkingSimulator.Core.Wrappers;

namespace ParkingSimulator.Core.Ioc
{
    public class Bootstrapper
    {
        public static void Init()
        {  
            DependencyInjector.RegisterType<IAccount, Account>();
            DependencyInjector.RegisterType<ITransactionLog, TransactionLog>();
            DependencyInjector.RegisterType<IParkingManager, ParkingManager>();
            
            DependencyInjector.RegisterInstance<ITimerWrapper>(new TimerWrapper(ParkingSimulatorSettings.RentalTimeSec*1000));
        }  
    }
}