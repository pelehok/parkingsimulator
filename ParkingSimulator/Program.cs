﻿using ParkingSimulator.Core;
using ParkingSimulator.ParkingMenu;

namespace ParkingSimulator
{
	internal class Program
	{
		public static void Main(string[] args)
		{
			MainMenu mainMenu = new MainMenu();
			mainMenu.PrintMenu();
		}
	}
}