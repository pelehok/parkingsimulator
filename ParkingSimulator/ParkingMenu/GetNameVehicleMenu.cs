using System;
using System.Linq;
using ParkingSimulator.Core;
using ParkingSimulator.Core.Transport.Abstract;
using ParkingSimulator.ParkingMenu.Abstract;
using ParkingSimulator.ParkingMenu.AddVehicleCommand;

namespace ParkingSimulator.ParkingMenu
{
	public class GetNameVehicleMenu 
	{
		public static Vehicle PrintMenu()
		{
			var parking = Parking.GetInstance();
			
			if (parking.NumberBusyCells() != 0)
			{
				Console.WriteLine("Which vehicle do you want? Choice number");

				var vehicles = parking.Cells.Select(x=>x.Vehicle).ToList();

				for (int i = 0; i < vehicles.Count; i++)
				{
					Console.WriteLine($"{i+1} {vehicles[i].ToString()}");
				}
				
				string userChoice;
				var vehicleIndex = -1;
				do
				{
					userChoice = Console.ReadLine();
				} while (!int.TryParse(userChoice, out vehicleIndex) || vehicleIndex > vehicles.Count ||
				         vehicleIndex <= 0);

				return vehicles[vehicleIndex - 1];

			}
			else
			{
				Console.WriteLine("Parking is empty");
			}

			return null;
		}
	}
}