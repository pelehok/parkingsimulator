using System;
using ParkingSimulator.Core;
using ParkingSimulator.Core.Exceptions.ParkingException;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.Core.Transport.Abstract;

namespace ParkingSimulator.ParkingMenu.AddVehicleCommand.Abstract
{
	public abstract class AddVehicle : ICommand
	{
		public abstract string Description { get; }
		public abstract bool Execute();

		protected void CreatVehicleMenu(Vehicle vehicle)
		{
			var parking = Parking.GetInstance();
			
			Console.WriteLine($"Do you want to add money in vehicle balance? Default value = {ParkingSimulatorSettings.DefaultVehicleBalance}. ({Yes_No[0]} , {Yes_No[1]})");
			if (UserChoiceYes_No())
			{
				Console.WriteLine($"How much?");
				var userBalance = UserBalance();
				vehicle.BankAccount.Deposit(userBalance);
			}
			else
			{
				vehicle.BankAccount.Deposit(ParkingSimulatorSettings.DefaultVehicleBalance);
			}

			var vehicleName = "";
			do
			{
				Console.WriteLine($"Add a name for the new vehicle. It must a vehicle identifier.");
				vehicleName = Console.ReadLine();
				Console.WriteLine($"Do you want to add a vehicle with name {vehicleName}. ({Yes_No[0]} , {Yes_No[1]})");
			} while (!UserChoiceYes_No());

			vehicle.VehicleName = vehicleName;
			try
			{
				parking.AddVehicle(vehicle);
				Console.WriteLine($"You add new vehicle to parking:{vehicle.ToString()}");
			}
			catch (ParkingFullException e)
			{
				Console.WriteLine(e.Message);
			}
		}
		
		private readonly string[] Yes_No = new[] {"Y", "N"};
		
		private double UserBalance()
		{
			string userChoice;
			var userBalance = 0.0;
			do
			{
				userChoice = Console.ReadLine();
			} while (!double.TryParse(userChoice, out userBalance) || userBalance <= 0);

			return userBalance;
		}

		private bool UserChoiceYes_No()
		{
			string userChoice;
			do
			{
				userChoice = Console.ReadLine();
			} while (!userChoice.ToUpper().Equals(Yes_No[0]) && !userChoice.ToUpper().Equals(Yes_No[1]));

			return userChoice.ToUpper().Equals(Yes_No[0]);
		}
		
	}
}