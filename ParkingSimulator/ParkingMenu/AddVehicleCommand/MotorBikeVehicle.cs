using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulator.ParkingMenu.AddVehicleCommand
{
	public class MotorBikeVehicle : AddVehicle
	{
		public override string Description { get; } = $"Motor bike";

		public override bool Execute()
		{
			var motorBike = new MotorBike(new Account(new TransactionLog()));

			CreatVehicleMenu(motorBike);
			return false;
		}
	}
}