using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulator.ParkingMenu.AddVehicleCommand
{
	public class BusVehicle : AddVehicle
	{
		public override string Description { get; } = $"Bus";

		public override bool Execute()
		{
			var bus = new Bus(new Account(new TransactionLog()));
			CreatVehicleMenu(bus);
			return false;
		}
	}
}