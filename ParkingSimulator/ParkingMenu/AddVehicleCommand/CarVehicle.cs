using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulator.ParkingMenu.AddVehicleCommand
{
	public class CarVehicle : AddVehicle
	{
		public override string Description { get; } = $"Car";

		public override bool Execute()
		{
			var car = new Car(new Account(new TransactionLog()));

			CreatVehicleMenu(car);
			return false;
		}
	}
}