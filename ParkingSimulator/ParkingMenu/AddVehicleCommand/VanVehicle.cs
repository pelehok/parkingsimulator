using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.ParkingMenu.AddVehicleCommand.Abstract;

namespace ParkingSimulator.ParkingMenu.AddVehicleCommand
{
	public class VanVehicle : AddVehicle
	{
		public override string Description { get; } = $"Van";

		public override bool Execute()
		{
			var van = new Van(new Account(new TransactionLog()));

			CreatVehicleMenu(van);
			return false;
		}
	}
}