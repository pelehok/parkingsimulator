using System;
using ParkingSimulator.ParkingMenu.Abstract;
using ParkingSimulator.ParkingMenu.AddVehicleCommand;

namespace ParkingSimulator.ParkingMenu
{
	public class AddVehicleMenu : Menu
	{
		private static readonly ICommand[] _commands = {
			new BusVehicle(),
			new CarVehicle(),
			new MotorBikeVehicle(),
			new VanVehicle(),
			new Back()
		};

		public AddVehicleMenu() : base(_commands){}
		
		public override void PrintMenu()
		{
			var isBack = false;
			while (!isBack)
			{
				Console.WriteLine();
				Console.WriteLine("Which vehicle do you want to add?");

				for (int i = 0; i < Commands.Length; i++)
				{
					Console.WriteLine("{0}. {1}", i + 1, Commands[i].Description);
				}
				var userChoice = GetUserChoice();
				Console.Clear();

				isBack = Commands[userChoice - 1].Execute();
			}
		}
	}
}