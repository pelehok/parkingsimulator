using System;
using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class ViewParkingTransaction : ICommand
	{
		public string Description { get; } = "Show parking transaction history for last minute";
		public bool Execute()
		{
			var parking = Parking.GetInstance();
			try
			{
				Console.WriteLine(parking.BankAccount.GetTransactionInfo());
			}
			catch (Exception e)
			{
				Console.WriteLine("History empty");
			}

			return false;
		}
	}
}