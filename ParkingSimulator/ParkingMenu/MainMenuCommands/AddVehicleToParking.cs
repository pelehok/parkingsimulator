namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class AddVehicleToParking : ICommand
	{
		public string Description { get; } = "Add vehicle to parking.";
		
		public bool Execute()
		{
			AddVehicleMenu menu = new AddVehicleMenu();
			menu.PrintMenu();

			return false;
		}
	}
}