using System;
using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class GetCellsInformation : ICommand
	{
		public string Description { get; } = "Get number free/busy parking spaces";
		public bool Execute()
		{
			var parking = Parking.GetInstance();
			
			Console.WriteLine($"Free {parking.NumberFreeCells()} parking spaces");
			Console.WriteLine($"Busy {parking.NumberBusyCells()} parking spaces");

			return false;
		}
	}
}