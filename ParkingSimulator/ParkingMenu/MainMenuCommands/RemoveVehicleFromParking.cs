using System;
using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class RemoveVehicleFromParking : ICommand
	{
		public string Description { get; } = "Remove vehicle from parking.";
		public bool Execute()
		{
			var parking = Parking.GetInstance();
			
			var vehicle = GetNameVehicleMenu.PrintMenu();
			if (vehicle == null) return false;
			
			Console.WriteLine($"Do you want to remove {vehicle.ToString()} vehicle?({Yes_No[0]}, {Yes_No[1]})");
			if (UserChoiceYes_No())
			{
				parking.RemoveVehicle(vehicle);
			}

			return false;
		}
		
		private readonly string[] Yes_No = new[] {"Y", "N"};

		private bool UserChoiceYes_No()
		{
			string userChoice;
			do
			{
				userChoice = Console.ReadLine();
			} while (!userChoice.ToUpper().Equals(Yes_No[0]) && !userChoice.ToUpper().Equals(Yes_No[1]));

			return userChoice.ToUpper().Equals(Yes_No[0]);
		}
		
	}
}