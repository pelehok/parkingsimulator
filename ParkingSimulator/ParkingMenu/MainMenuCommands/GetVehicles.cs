using System;
using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class GetVehicles : ICommand
	{
		public string Description { get; } = "Get information about all vehicles in parking.";
		public bool Execute()
		{
			var parking = Parking.GetInstance();

			foreach (var cell in parking.Cells)
			{
				Console.WriteLine(cell.Vehicle.ToString());
			}

			return false;
		}
	}
}