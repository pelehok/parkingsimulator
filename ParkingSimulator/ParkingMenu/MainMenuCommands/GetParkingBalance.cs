using System;
using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class GetParkingBalance : ICommand
	{
		public string Description { get; } = "Get parking balance.";
		public bool Execute()
		{
			var parking = Parking.GetInstance();

			Console.WriteLine($"Parking balance = {parking.BankAccount.Balance}");
			
			return false;
		}
	}
}