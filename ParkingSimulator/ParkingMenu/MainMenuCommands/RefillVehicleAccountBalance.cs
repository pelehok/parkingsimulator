using System;
using System.Linq;
using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu.MainMenuCommands
{
	public class RefillVehicleAccountBalance : ICommand
	{
		public string Description { get; } = "Refill vehicle the account balance.";
		public bool Execute()
		{
			var parking = Parking.GetInstance();
			
			var vehicle = GetNameVehicleMenu.PrintMenu();
			if (vehicle == null) return false;

			Console.WriteLine($"Do you want to refill {vehicle.ToString()} vehicle account balance?({Yes_No[0]}, {Yes_No[1]})");
			if (UserChoiceYes_No())
			{
				parking.RemoveVehicle(vehicle);
				Console.WriteLine("How much?");
				var balance = UserBalance();
				parking.Cells.First(x => x.Vehicle.Equals(vehicle)).Vehicle.BankAccount.Deposit(balance);
			}

			return false;
		}
		
		private readonly string[] Yes_No = new[] {"Y", "N"};

		private bool UserChoiceYes_No()
		{
			string userChoice;
			do
			{
				userChoice = Console.ReadLine();
			} while (!userChoice.ToUpper().Equals(Yes_No[0]) && !userChoice.ToUpper().Equals(Yes_No[1]));

			return userChoice.ToUpper().Equals(Yes_No[0]);
		}
		
		private double UserBalance()
		{
			string userChoice;
			var userBalance = 0.0;
			do
			{
				userChoice = Console.ReadLine();
			} while (!double.TryParse(userChoice, out userBalance) || userBalance <= 0);

			return userBalance;
		}
	}
}