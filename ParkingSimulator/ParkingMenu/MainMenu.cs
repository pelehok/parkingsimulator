using System;
using ParkingSimulator.ParkingMenu.Abstract;
using ParkingSimulator.ParkingMenu.MainMenuCommands;

namespace ParkingSimulator.ParkingMenu
{
	public class MainMenu : Menu
	{
		private static ICommand[] _commands = {
			new GetParkingBalance(),
			new GetCellsInformation(),
			new GetVehicles(),
			new AddVehicleToParking(),
			new RemoveVehicleFromParking(),
			new RefillVehicleAccountBalance(),
			//new ViewTransactionHistory(),
			new ViewParkingTransaction()
		};

		public MainMenu() : base(_commands){}
		
		public override void PrintMenu()
		{
			Console.WriteLine("Welcome to Parking Simulator.");
			while (true)
			{
				Console.WriteLine();
				Console.WriteLine("What do you want to do?");

				for (int i = 0; i < Commands.Length; i++)
				{
					Console.WriteLine("{0}. {1}", i + 1, Commands[i].Description);
				}
				var userChoice = GetUserChoice();
				Console.Clear();

				Commands[userChoice - 1].Execute();
			}
		}
	}
}