using ParkingSimulator.Core;

namespace ParkingSimulator.ParkingMenu
{
	public interface ICommand
	{
		string Description { get; }
		bool Execute();
	}
}