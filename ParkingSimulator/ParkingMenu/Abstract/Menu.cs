using System;

namespace ParkingSimulator.ParkingMenu.Abstract
{
	public abstract class Menu
	{
		protected ICommand[] Commands { get; set; }

		public Menu(ICommand[] commands)
		{
			Commands = commands;
		}

		protected int GetUserChoice()
		{
			string userChoice;
			var commandIndex = -1;
			do
			{
				userChoice = Console.ReadLine();
			} while (!int.TryParse(userChoice, out commandIndex) || commandIndex > Commands.Length ||
			         commandIndex <= 0);

			return commandIndex;
		}

		public abstract void PrintMenu();
	}
}