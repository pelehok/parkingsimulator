using NUnit.Framework;
using ParkingSimulator.Core;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.ParkingLogic;
using ParkingSimulator.Core.Transport;

namespace ParkingSimulator.Tests.ParkingTests
{
    [TestFixture]
    public class ParkingCellTests
    {
        [Test]
        public void CreateInstance()
        {
            const double inputRentalRate = 0.1;
            var inputVehicle = new Car(new Account(new TransactionLog()));
            var parkingCell = new ParkingCell(inputVehicle,inputRentalRate);
            
            Assert.IsFalse(parkingCell.IsHavePenalty);
            Assert.AreEqual(inputVehicle,parkingCell.Vehicle);
        }

        [Test]
        public void TransferToAccount()
        {
            const double inputRentalRate = 1;
            
            var account = new Account(new TransactionLog());
            account.Deposit(10);
            var inputVehicle = new Car(account);
            
            var transferAccount = new Account(new TransactionLog());
            transferAccount.Deposit(10);
            
            var parkingCell = new ParkingCell(inputVehicle,inputRentalRate);
            parkingCell.RentPayment(transferAccount);
            
            Assert.IsFalse(parkingCell.IsHavePenalty);
            Assert.AreEqual(9,parkingCell.Vehicle.BankAccount.Balance);
            Assert.AreEqual(11,transferAccount.Balance);
        }
        
        [Test]
        public void RentalRateBiggerThenCurrentBalance()
        {
            const double inputRentalRate = 2;
            
            var account = new Account(new TransactionLog());
            account.Deposit(1);
            var inputVehicle = new Car(account);
            
            var transferAccount = new Account(new TransactionLog());
            transferAccount.Deposit(10);
            
            var parkingCell = new ParkingCell(inputVehicle,inputRentalRate);
            parkingCell.RentPayment(transferAccount);
            
            Assert.IsTrue(parkingCell.IsHavePenalty);
            Assert.AreEqual(account.Balance,parkingCell.Vehicle.BankAccount.Balance);
            Assert.AreEqual(10,transferAccount.Balance);
        }
        
        [Test]
        public void CorrectGetPenalty()
        {
            const double inputRentalRate = 2;
            
            var account = new Account(new TransactionLog());
            account.Deposit(1);
            var inputVehicle = new Car(account);
            
            var transferAccount = new Account(new TransactionLog());
            transferAccount.Deposit(10);
            
            var parkingCell = new ParkingCell(inputVehicle,inputRentalRate);
            parkingCell.RentPayment(transferAccount);
            inputVehicle.BankAccount.Deposit(10);
            parkingCell.RentPayment(transferAccount);
            
            Assert.IsFalse(parkingCell.IsHavePenalty);
            //1+10 = 11 - 4*1.5 = 6
            Assert.AreEqual(11-4*ParkingSimulatorSettings.PenaltyCoefficient,parkingCell.Vehicle.BankAccount.Balance);
            Assert.AreEqual(16,transferAccount.Balance);
        }
        
        [Test]
        public void DontGetRentManyTimes()
        {
            const double inputRentalRate = 2;
            
            var account = new Account(new TransactionLog());
            account.Deposit(1);
            var inputVehicle = new Car(account);
            
            var transferAccount = new Account(new TransactionLog());
            transferAccount.Deposit(10);
            
            var parkingCell = new ParkingCell(inputVehicle,inputRentalRate);
            parkingCell.RentPayment(transferAccount);
            parkingCell.RentPayment(transferAccount);
            inputVehicle.BankAccount.Deposit(10);
            parkingCell.RentPayment(transferAccount);
            
            Assert.IsFalse(parkingCell.IsHavePenalty);
            //1+10 = 11 - 6*1.5 = 6
            Assert.AreEqual(11-6*ParkingSimulatorSettings.PenaltyCoefficient,parkingCell.Vehicle.BankAccount.Balance);
            Assert.AreEqual(19,transferAccount.Balance);
        }
    }
}