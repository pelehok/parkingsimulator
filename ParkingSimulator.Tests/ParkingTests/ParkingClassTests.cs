using System;
using System.Collections;
using System.Collections.Generic;
using Castle.Core.Internal;
using Moq;
using NUnit.Framework;
using ParkingSimulator.Core;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Exceptions.ParkingException;
using ParkingSimulator.Core.Ioc;
using ParkingSimulator.Core.ParkingLogic;
using ParkingSimulator.Core.ParkingLogic.Abstract;
using ParkingSimulator.Core.Transport;
using ParkingSimulator.Core.Transport.Abstract;
using ParkingSimulator.Core.Wrappers;
using Unity.Injection;

namespace ParkingSimulator.Tests.ParkingTests
{
    [TestFixture]
    public class ParkingClassTests
    {
        private Mock<ITimerWrapper> _timerWrapperMock;
        private Mock<IAccount> _account;
        private Mock<IParkingManager> _parkingManager;

        [SetUp]
        public void SetUp()
        {
            _timerWrapperMock = new Mock<ITimerWrapper>(MockBehavior.Strict);
            _timerWrapperMock.Setup(timer => timer.Start());

            _account = new Mock<IAccount>(MockBehavior.Strict);
            _account.Setup(account => account.Deposit(It.IsAny<double>()));

            _parkingManager = new Mock<IParkingManager>(MockBehavior.Strict);
            _parkingManager.Setup(parkingManager =>
                parkingManager.SetUpParkingData(It.IsAny<List<ParkingCell>>(), It.IsAny<IAccount>()));
            
            DependencyInjector.RegisterType<IAccount>(new InjectionFactory(c => _account.Object));
            DependencyInjector.RegisterType<ITimerWrapper>(new InjectionFactory(c => _timerWrapperMock.Object));
            DependencyInjector.RegisterType<IParkingManager>(new InjectionFactory(c => _parkingManager.Object));
        }

        [Test]
        public void CreateInstance()
        {
            var parking = DependencyInjector.Resolve<Parking>();
            Assert.IsEmpty(parking.Cells);
            Assert.NotNull(parking.BankAccount);

            _account.Verify(x => x.Deposit(It.IsIn(ParkingSimulatorSettings.DefaultParkingBalance)), Times.Once);
            _parkingManager.Verify(x => x.SetUpParkingData(
                    It.Is<List<ParkingCell>>(cells => cells.IsNullOrEmpty()), 
                    It.Is<IAccount>(account => account != null)),
                Times.Once);
        }
        
        [Test]
        public void AddingVehicle()
        {
            var parking = DependencyInjector.Resolve<Parking>();

            var inputVehicle = DependencyInjector.Resolve<Car>();
            parking.AddVehicle(inputVehicle);
            
            Assert.AreEqual(1,parking.Cells.Count);
            Assert.AreEqual(inputVehicle,parking.Cells[0].Vehicle);
        }
        
        [Test]
        public void AddingVehicleInFullParking()
        {
            var parking = DependencyInjector.Resolve<Parking>();

            var inputVehicle = DependencyInjector.Resolve<Car>();
            for(int i = 0;i<ParkingSimulatorSettings.MaxParkingSize;i++)
                parking.AddVehicle(inputVehicle);

            try
            {
                parking.AddVehicle(inputVehicle);
                Assert.Fail("Parking can't add");
            }
            catch (ParkingFullException e)
            {
                Assert.Pass();
            }
            
            Assert.AreEqual(1,parking.Cells.Count);
            Assert.AreEqual(inputVehicle,parking.Cells[0].Vehicle);
        }
        
        [Test]
        public void RemoveVehicleFromEmptyParking()
        {
            var parking = DependencyInjector.Resolve<Parking>();
            var inputVehicle = DependencyInjector.Resolve<Car>();

            try
            {
                parking.RemoveVehicle(inputVehicle);
                Assert.Fail("Parking empty");
            }
            catch (EmptyParkingException e)
            {
                Assert.Pass();
            }
            
            Assert.IsEmpty(parking.Cells);
        }
        
        [Test]
        public void ParkingDoesNotHaveSpecialVehicle()
        {
            var parking = DependencyInjector.Resolve<Parking>();
            var inputVehicle = DependencyInjector.Resolve<Car>();

            parking.AddVehicle(inputVehicle);
            try
            {
                var inputAnotherVehicle = DependencyInjector.Resolve<Car>();
                parking.RemoveVehicle(inputAnotherVehicle);
                Assert.Fail("Parking doesn't have this vehicle");
            }
            catch (ArgumentException e)
            {
                Assert.Pass();
            }

            Assert.AreEqual(1, parking.Cells);
        }
        
        [Test]
        public void RemoveVehicleWithPenalty()
        {
            var parking = DependencyInjector.Resolve<Parking>();
            var inputVehicle = new Car(new Account(new TransactionLog()));
            parking.AddVehicle(inputVehicle);
            parking.Cells[0].RentPayment(new Account(new TransactionLog()));
            
            try
            {
                parking.RemoveVehicle(inputVehicle);
                Assert.Fail("Parking doesn't have this vehicle");
            }
            catch (VehicleHasPenaltyException e)
            {
                Assert.Pass();
            }

            Assert.AreEqual(1, parking.Cells);
        }
        
        [Test]
        public void RemoveVehicle()
        {
            var parking = DependencyInjector.Resolve<Parking>();
            var inputVehicle = DependencyInjector.Resolve<Car>();
            parking.AddVehicle(inputVehicle);
            
            parking.RemoveVehicle(inputVehicle);

            Assert.IsEmpty(parking.Cells);
        }

        private static IEnumerable VehiclesDataForFreeCells
        {
            get
            {
                yield return new TestCaseData(new List<Vehicle>()).Returns(10);
                yield return new TestCaseData(new List<Vehicle>()
                {
                    new Bus(new Account(new TransactionLog())),
                    new Bus(new Account(new TransactionLog())),
                }).Returns(8);
            }
        }
        
        [Test,TestCaseSource(nameof(VehiclesDataForFreeCells))]
        public int CheckFreeCells(List<Vehicle> vehicles)
        {
            var parking = DependencyInjector.Resolve<Parking>();

            foreach (var vehicle in vehicles)
            {
                parking.AddVehicle(vehicle);
            }
            
            return parking.NumberFreeCells();

        }

        private static IEnumerable VehiclesDataForBusyCells
        {
            get
            {
                yield return new TestCaseData(new List<Vehicle>()).Returns(0);
                yield return new TestCaseData(new List<Vehicle>()
                {
                    new Bus(new Account(new TransactionLog())),
                    new Bus(new Account(new TransactionLog())),
                }).Returns(2);
            }
        }
        
        [Test,TestCaseSource(nameof(VehiclesDataForBusyCells))]
        public int CheckBusyCells(List<Vehicle> vehicles)
        {
            var parking = DependencyInjector.Resolve<Parking>();

            foreach (var vehicle in vehicles)
            {
                parking.AddVehicle(vehicle);
            }
            
            return parking.NumberBusyCells();

        }
    }
}