using Moq;
using NUnit.Framework;
using ParkingSimulator.Core.Banking;
using ParkingSimulator.Core.Banking.Abstract;
using ParkingSimulator.Core.Banking.Transactions;
using ParkingSimulator.Core.Banking.Transactions.Abstract;
using ParkingSimulator.Core.Exceptions.BankingExceptions;

namespace ParkingSimulator.Tests.BankingTest
{
    [TestFixture]
    public class AccountTests
    {
        private Mock<ITransactionLog> _transactionLog;
        private IAccount _account;

        [SetUp]
        public void SetUp()
        {
            _transactionLog = new Mock<ITransactionLog>(MockBehavior.Strict);
            _account = new Account(_transactionLog.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _transactionLog.VerifyAll();
        }

        #region Deposit
        [Test]
        public void Deposit_InvalidAmount()
        {
            const int inputValue = -1;
            Assert.Throws<InvalidAmountException>(() => 
            {
                _account.Deposit(inputValue);
            },"Must throw exception when input invalid value");
        }
        
        [Test]
        public void Deposit_CheckBalance()
        {
            const int inputValue = 10;
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<DepositTransaction>()));
                
            _account.Deposit(inputValue);
            
            Assert.AreEqual(inputValue,_account.Balance,"Incorrect Balance");
        }
        
        [Test]
        public void Deposit_CheckAddingTransaction()
        {
            const int inputValue = 10;
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<DepositTransaction>()))
                .Callback<ITransaction>((transaction) =>
                {
                    Assert.AreEqual(inputValue,transaction.Amount, "Incorrect transaction amount");
                });

            _account.Deposit(inputValue);
            
            _transactionLog.Verify(transactionLog => transactionLog.AddTransaction(It.IsAny<DepositTransaction>()), Times.Once,
                "Adding transaction must call only once");
            Assert.AreEqual(inputValue,_account.Balance,"Incorrect Balance");
        }
        #endregion

        #region Withdraw
        [Test]
        public void Withdraw_InvalidAmount()
        {
            const int inputValue = -1;
            Assert.Throws<InvalidAmountException>(() => 
            {
                _account.Withdraw(inputValue);
            },"Must throw exception when input invalid value");
        }
        
        [Test]
        public void Withdraw_AmountLessBalance()
        {
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<ITransaction>()));
            _account.Deposit(10);
            
            const int inputValue = 20;
            Assert.Throws<InvalidAmountException>(() => 
            {
                _account.Withdraw(inputValue);
            },"Must throw exception when the amount is less than the current balance");
        }
        
        [Test]
        public void Withdraw_CheckBalance()
        {
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<ITransaction>()));
            _account.Deposit(50);
            
            const int inputValue = 10;
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<WithdrawTransaction>()));
            
            _account.Withdraw(inputValue);
            
            Assert.AreEqual(40,_account.Balance,"Incorrect Balance");
        }
        
        [Test]
        public void Withdraw_CheckAddingTransaction()
        {
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<ITransaction>()));
            _account.Deposit(50);
            
            const int inputValue = 10;
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<WithdrawTransaction>()))
                .Callback<ITransaction>((transaction) =>
                {
                    Assert.AreEqual(inputValue,transaction.Amount, "Incorrect transaction amount");
                });

            _account.Withdraw(inputValue);
            
            _transactionLog.Verify(transactionLog => transactionLog.AddTransaction(It.IsAny<WithdrawTransaction>()), Times.Once,
                "Adding transaction must call only once");
            Assert.AreEqual(40,_account.Balance,"Incorrect Balance");
        }
        #endregion

        #region TransferFunds

        [Test]
        public void TransferFunds_CheckBalance()
        {
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<ITransaction>()));
            _account.Deposit(50);
            
            const int inputValue = 10;
            
            var accountMock = new Mock<IAccount>(MockBehavior.Strict);
            accountMock.Setup(account => account.Deposit(inputValue));

            _account.TransferFunds(accountMock.Object, inputValue);
            
            Assert.AreEqual(40,_account.Balance);
        }
        
        [Test]
        public void TransferFunds_CheckAddingTransaction()
        {
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<ITransaction>()));
            _account.Deposit(50);
            
            const int inputValue = 10;
            _transactionLog.Setup(transactionLog => transactionLog.AddTransaction(It.IsAny<WithdrawTransaction>()))
                .Callback<ITransaction>((transaction) =>
                {
                    Assert.AreEqual(inputValue,transaction.Amount, "Incorrect transaction amount");
                });
            
            var accountMock = new Mock<IAccount>(MockBehavior.Strict);
            accountMock.Setup(account => account.Deposit(inputValue));

            _account.TransferFunds(accountMock.Object, inputValue);
            
            _transactionLog.Verify(transactionLog => transactionLog.AddTransaction(It.IsAny<WithdrawTransaction>()), Times.Once,
                "Adding transaction must call only once");
            Assert.AreEqual(40,_account.Balance,"Incorrect Balance");
        }

        #endregion
    }
}